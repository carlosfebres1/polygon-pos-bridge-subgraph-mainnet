export const RecordStatus__PENDING = "PENDING"
export const RecordStatus__UNKNOWN = "UNKNOWN"
export const RecordStatus__ACTION_REQUIRED = "ACTION_REQUIRED"
export const RecordStatus__SUCCESS = "SUCCESS"
export const RecordStatus__ERROR = "ERROR"

export const MINDS_ROOT_TOKEN = "0x8bda9f5c33fbcb04ea176ea5bc1f5102e934257f"
export const ERC20_PREDICATE = "0xdd6596f2029e6233deffaca316e6a95217d4dc34"
