import {NewHeaderBlock} from '../generated/RootChain/RootChain';
import { HeaderBlock } from '../generated/schema';

export function handleNewHeader(event: NewHeaderBlock): void {
    const headerBlock = new HeaderBlock(event.transaction.hash.toHex());
    headerBlock.end = event.params.end;
    headerBlock.save()
}
