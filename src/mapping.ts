import {LockedERC20} from "../generated/ERC20Predicate/ERC20Predicate"
import {Deposit} from "../generated/schema"
import {log} from "@graphprotocol/graph-ts";
import {getEventId, getOrCreateWallet} from "./helpers";
import {MINDS_ROOT_TOKEN, RecordStatus__PENDING} from "./constants";

export function handleLockedERC20(event: LockedERC20): void {
    if (event.params.rootToken.toHex() != MINDS_ROOT_TOKEN) return;
    log.warning("Tokens Locked: depositor {}", [event.params.depositor.toHex()])

    getOrCreateWallet(event.params.depositor.toHex())

    const entity = new Deposit(getEventId(event));
    entity.wallet = event.params.depositor.toHex()
    entity.status = RecordStatus__PENDING
    entity.timestamp = event.block.timestamp
    entity.amount = event.params.amount
    entity.txHash = event.transaction.hash
    entity.txBlock = event.block.number
    entity.save()
}
