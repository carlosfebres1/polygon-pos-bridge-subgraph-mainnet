import {ethereum} from "@graphprotocol/graph-ts/index";
import {Wallet} from "../generated/schema";


export function getEventId(event: ethereum.Event): string {
    return event.transaction.hash.toHex() + "-" + event.logIndex.toString()
}

export function getOrCreateWallet(address: string): Wallet {
    let wallet = Wallet.load(address)
    if (!wallet) {
        wallet = new Wallet(address)
        wallet.save()
    }
    // Create new Wallet
    return wallet
}
