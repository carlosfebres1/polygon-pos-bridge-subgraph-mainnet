import { log } from "@graphprotocol/graph-ts/index";
import { getEventId, getOrCreateWallet } from "./helpers";
import { ERC20_PREDICATE, RecordStatus__SUCCESS } from "./constants";
import { Withdraw } from "../generated/schema";
import { Transfer } from "../generated/MindsToken/ERC20";

export function handleExitedERC20(event: Transfer): void {
  if (event.params.from.toHex() != ERC20_PREDICATE) return;
  log.warning("Exited Tokens. exitor: {}", [event.params.to.toHex()]);
  getOrCreateWallet(event.params.to.toHex());

  const entity = new Withdraw(getEventId(event));
  entity.wallet = event.params.to.toHex();
  entity.status = RecordStatus__SUCCESS;
  entity.timestamp = event.block.timestamp;
  entity.amount = event.params.value;
  entity.txHash = event.transaction.hash;
  entity.txData = event.transaction.input;
  entity.txBlock = event.block.number;
  entity.exitData = event.transaction.input;
  entity.save();
}
